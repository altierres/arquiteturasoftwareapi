<!DOCTYPE html>
<html lang="pt-br">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Pedidos Service</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ asset('css/scrolling-nav.css') }}" rel="stylesheet">

  </head>

  <body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="{{ url('/') }}">Pedidos Service</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
      </div>
    </nav>

    <section>
        <div class="container text-center">
            <h4><p>Deseja pesquisar <b>PRODUTOS</b> diversificados através de seu código identificador global.</p>
            <p>Deseja encontrar os dados dos <b>FORNECEDORES</b> cadastrados em todo Brasil.</p>
            <p>A PedidosNow oferece um serviço para integração com seu sistema, utilizando padrão <b>JSON</b> como sua principal forma comunicação. </p></h4>    
        </div>
    </section>

    <header class="bg-primary text-white">
      <div class="container text-center">
        <h1>Serviços de Produtos</h1>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">Função</th>
              <th scope="col">Método</th>
              <th scope="col">Exemplo</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Pesquisar todos os produtos</td>
              <td>GET</td>
              <td>localhost:8000/api/produtos/</td>
            </tr>
            <tr>
              <td>Pesquisar produto pelo ID</td>
              <td>GET</td>
              <td>localhost:8000/api/produtos/1</td>
            </tr>
            
          </tbody>
        </table>
      </div>
      <br>
      <br>
      <br>
      <div class="container text-center">
        <h1>Serviços de Fornecedores</h1>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">Função</th>
              <th scope="col">Método</th>
              <th scope="col">Exemplo</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Pesquisar todos os fornecedores</td>
              <td>GET</td>
              <td>localhost:8000/api/fornecedores/</td>
            </tr>
            <tr>
              <td>Pesquisar fornecedor pelo ID</td>
              <td>GET</td>
              <td>localhost:8000/api/fornecedores/1</td>
            </tr>
            
          </tbody>
        </table>
      </div>
      
    </header>

    <section style="background-color: #7cf66a">
        <div class="container text-center">
        <h1>Possíveis Retornos</h1>

        <table class="table">
          <thead>
            <tr>
              <th scope="col">Código</th>
              <th scope="col">Significado</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>200 OK</td>
              <td>Requisição processada e realizada com sucesso</td>
            </tr>
            <tr>
              <td>400 Bad Request</td>
              <td>Requisição processada mas nada foi encontrado</td>
            </tr>
            <tr>
              <td>404 Not Found</td>
              <td>Não foi possível encontrar oque deseja</td>
            </tr>
            
          </tbody>
        </table>
      </div>
    </section>

      

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; PedidosService</p>
      </div>
      <!-- /.container -->
    </footer>


  </body>

    <script>

      
    </script>

</html>