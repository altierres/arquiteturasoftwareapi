<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{

	protected $hidden = [
        'category_id', 'provider_id','imagem'
    ];

	protected $fillable = [
                'name',
                'description',
                'imagem',
                'purchase_value',
                'sale_value'];

}
